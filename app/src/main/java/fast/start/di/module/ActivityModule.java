package fast.start.di.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import fast.start.di.qualifier.PerActivity;
import fast.start.presenter.LoginActivityPresenter;
import fast.start.presenter.impl.LoginActivityPresenterImpl;
import fast.start.ui.Navigator;

/**
 * Created by danila on 26.06.16.
 */
@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return new Navigator(activity);
    }

    @Provides
    @PerActivity
    LoginActivityPresenter provideLoginActivityPresenter(LoginActivityPresenterImpl presenter) {
        return presenter;
    }

}
