package fast.start.di.component;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Component;
import fast.start.di.module.AppModule;
import fast.start.di.module.UtilsModule;
import fast.start.utils.RxUtil;
import fast.start.utils.SharedPreferencesUtils;

/**
 * Created by danila on 12.08.16.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        UtilsModule.class
})
public interface AppComponent {

    Application getApplication();

    Resources getResource();

    Context getContext();

    SharedPreferencesUtils getSharedPreferencesUtils();

    RxUtil getRxUtil();
}
