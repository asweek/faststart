package fast.start.di.component;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;

import dagger.Component;
import fast.start.di.module.ActivityModule;
import fast.start.di.qualifier.PerActivity;
import fast.start.ui.Navigator;
import fast.start.ui.activity.LoginActivity;
import fast.start.utils.RxUtil;
import fast.start.utils.SharedPreferencesUtils;

/**
 * Created by danila on 12.08.16.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity getActivity();

    Resources getResource();

    Navigator getNavigator();

    Context getContext();

    SharedPreferencesUtils getSharedPreferencesUtils();

    RxUtil getRxUtil();

    void inject(LoginActivity loginActivity);

}
