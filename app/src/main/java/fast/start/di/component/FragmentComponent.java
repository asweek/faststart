package fast.start.di.component;

import android.app.Activity;

import dagger.Component;
import fast.start.di.module.FragmentModule;
import fast.start.di.qualifier.PerFragment;
import fast.start.ui.Navigator;

/**
 * Created by danila on 12.08.16.
 */
@PerFragment
@Component(dependencies = {ActivityComponent.class}, modules = {FragmentModule.class})
public interface FragmentComponent {
    Activity getActivity();

    Navigator getNavigator();

}
