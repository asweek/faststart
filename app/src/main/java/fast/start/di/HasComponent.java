package fast.start.di;

/**
 * Created by danila on 12.08.16.
 */
public interface HasComponent<T> {

    T getComponent();

}
