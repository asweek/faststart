package fast.start.listener;

/**
 * Created by danila on 26.08.16.
 */
public interface OnAdapterListener {
    <T> void onClick(T t);
}
