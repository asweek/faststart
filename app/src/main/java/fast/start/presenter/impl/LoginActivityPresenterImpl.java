package fast.start.presenter.impl;

import javax.inject.Inject;

import fast.start.presenter.LoginActivityPresenter;
import fast.start.ui.Navigator;
import fast.start.utils.SharedPreferencesUtils;

/**
 * Created by danila on 15.08.16.
 */
public class LoginActivityPresenterImpl extends LoginActivityPresenter {

    private Navigator navigator;
    private SharedPreferencesUtils preferencesUtils;

    @Inject
    public LoginActivityPresenterImpl(Navigator navigator, SharedPreferencesUtils preferencesUtils) {
        this.navigator = navigator;
        this.preferencesUtils = preferencesUtils;
    }

    @Override
    public void login(String login, String password) {
        preferencesUtils.createUserLoginSession(login, password);
    }
}
