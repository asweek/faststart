package fast.start;

import android.app.Application;

import fast.start.di.HasComponent;
import fast.start.di.component.AppComponent;
import fast.start.di.component.DaggerAppComponent;
import fast.start.di.module.AppModule;

/**
 * Created by danila on 12.08.16.
 */
public class FastStartApp extends Application implements HasComponent<AppComponent> {
    private AppComponent mAppComponent;
    private static FastStartApp instance;

    public static FastStartApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initializeInjector();
    }

    private void initializeInjector() {

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @Override
    public AppComponent getComponent() {
        return mAppComponent;
    }
}
