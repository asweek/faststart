package fast.start.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import fast.start.di.HasComponent;
import fast.start.di.component.DaggerFragmentComponent;
import fast.start.di.component.FragmentComponent;
import fast.start.di.module.FragmentModule;
import fast.start.ui.activity.BaseActivity;

/**
 * Created by danila on 12.08.16.
 */
public class BaseFragment extends Fragment implements HasComponent<FragmentComponent> {

    private FragmentComponent fragmentComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFragmentComponent();
    }

    private void initFragmentComponent() {
        BaseActivity activity = (BaseActivity) getActivity();
        fragmentComponent = DaggerFragmentComponent.builder()
                .activityComponent(activity.getComponent())
                .fragmentModule(new FragmentModule())
                .build();
    }

    @Override
    public FragmentComponent getComponent() {
        return fragmentComponent;
    }

    public void setupTitle(String title) {
        getActivity().setTitle(title);
    }
}
